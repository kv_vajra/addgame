package com.example.sanjiv.LaJodh;

import android.app.Activity;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.sanjiv.braintrainer.R;

import java.util.ArrayList;
import java.util.Random;

public class GamePage extends Activity {


    ArrayList<Integer> answer= new ArrayList<>();
    int locationOfCorrectAnswer;
    TextView timeup;
    TextView question;
    int score=0;
    int questionCount=0;
    TextView result;
    Button option1;
    Button option2;
    Button option3;
    Button option4;
    TextView counter;
    CountDownTimer ctimer;

    public void playAgain(final View view){
        timeup.setText("");
        score=0;
        questionCount=0;
        result.setText("0 / 0");
        counter.setText("60sec");
        questionSpawner();
        ctimer = new CountDownTimer(60100, 1000) {

            @Override
            public void onTick(long l) {
                counter.setText(String.valueOf(l/1000) + " sec");
            }


            @Override
            public void onFinish() {
                counter.setText("0 sec");
                Intent intent = new Intent(GamePage.this, PlayAgain.class);
                intent.putExtra("score",score);
                intent.putExtra("questionCount",questionCount);
                startActivity(intent);
                finish();
            }
        }.start();
    }

    public void questionSpawner(){
        Random random = new Random();
        int a = random.nextInt(100);
        int b = random.nextInt(100);
        while (a == 0 || b == 0){
            a=random.nextInt(100);
            b=random.nextInt(100);
        }
        option1 = (Button) findViewById(R.id.option1);
        option2 = (Button) findViewById(R.id.option2);
        option3 = (Button) findViewById(R.id.option3);
        option4 = (Button) findViewById(R.id.option4);


        question.setText(String.valueOf(a) + " + " + String.valueOf(b));
        locationOfCorrectAnswer=random.nextInt(4);

        answer.clear();

        int incorrectAnswer;
        for (int i=0; i<4; i++){
            if (i==locationOfCorrectAnswer){
                answer.add(a+b);
            }else {
                incorrectAnswer = random.nextInt(200);
                while (incorrectAnswer==a+b || incorrectAnswer==0){
                    incorrectAnswer=random.nextInt(200);
                }
                answer.add(incorrectAnswer);
            }
        }
        option1.setText(Integer.toString(answer.get(0)));
        option2.setText(Integer.toString(answer.get(1)));
        option3.setText(Integer.toString(answer.get(2)));
        option4.setText(Integer.toString(answer.get(3)));


    }

    public void chooseAnswer(View view){
        Log.i("Tag", (String) view.getTag ());
        if (view.getTag().toString().equals(Integer.toString(locationOfCorrectAnswer))){
            Log.i("Correct","Correct");
            timeup.setText("Correct");
            score++;
            questionCount++;
            result.setText(Integer.toString(score)+" / " +Integer.toString(questionCount));



        }else {
            timeup.setText("Wrong Answer");
            questionCount++;
            result.setText(Integer.toString(score)+" / "+Integer.toString(questionCount));
        }
        questionSpawner();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_page);

        timeup = (TextView) findViewById(R.id.timeUp);
        counter= (TextView) findViewById(R.id.timer);
        question = (TextView) findViewById(R.id.question);
        result= (TextView) findViewById(R.id.result);


        playAgain(findViewById(R.id.result));

    }

    @Override
    public void onBackPressed() {
        ctimer.cancel();
        super.onBackPressed();
        this.finish();
    }
}
