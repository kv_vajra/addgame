package com.example.sanjiv.LaJodh;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.example.sanjiv.LaJodh.Constants.SharedPreferenceConstants;
import com.example.sanjiv.braintrainer.R;

public class PlayAgain extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_again);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(PlayAgain.this);
        SharedPreferences.Editor editor = prefs.edit();

        TextView scoreTV = (TextView) findViewById(R.id.importScore);
        TextView questionTV = (TextView) findViewById(R.id.importQusetion);
        TextView totalScoreTV = (TextView) findViewById(R.id.totalScoreCount);
        TextView highScoreTV = (TextView) findViewById(R.id.highScoreTV);

        int score;
        int question;
        double averagePer;
        double average;
        double realScore;

        Bundle get = getIntent().getExtras();
        score = get.getInt("score");
        question = get.getInt("questionCount");

        scoreTV.setText(Integer.toString(score));
        questionTV.setText(Integer.toString(question));
        averagePer = (score * 100) ;
        average = averagePer / question;

        if (average >= 50){
            realScore = score * average;
        }else {
            realScore =score * (average/2);
        }

        totalScoreTV.setText(String.valueOf((int) realScore));

        if (prefs.contains(SharedPreferenceConstants.highScore)) {
            int prevHighScore = prefs.getInt(SharedPreferenceConstants.highScore, 0);
            if (realScore > prevHighScore) {
                highScoreTV.setText(String.valueOf((int) realScore));
                editor.putInt(SharedPreferenceConstants.highScore, (int) realScore);
                editor.apply();
            } else
                highScoreTV.setText(String.valueOf(prevHighScore));

        } else {
            highScoreTV.setText(String.valueOf((int) realScore));
            editor.putInt(SharedPreferenceConstants.highScore, (int)realScore);
            editor.apply();
        }

    }
}
